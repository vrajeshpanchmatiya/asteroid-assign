import { asteroidAllUserApi } from "../Services/asteroidAllUserApi";
export const asteroidAllIdAction = async () => {
  const detail = await asteroidAllUserApi();
  return detail.data;
};
