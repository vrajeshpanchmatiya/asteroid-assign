import { asteroidType } from "./Type/asteroidType";
import { asteroidUserApi } from "../Services/asteroidUserApi";
export const asteroidIdAction = (id) => {
  return async (dispatch) => {
    const details = await asteroidUserApi(id);
    dispatch({ type: asteroidType, payload: details.data });
  };
};
