import { Box, Typography } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import "../Common.scss";
const NasaUserDetail = () => {
  // useSelector for Nasa user form
  const info = useSelector((state) => {
    return state.data;
  });
  // Nasa User Details
  return (
    <div className="div-screen">
      <Box className="box-form">
        <h1>Nasa User Detail</h1>
        <Typography>
          <b>Name: </b>
          {info.name}
        </Typography>
        <Typography>
          <b>URL: </b>
          {info.nasa_jpl_url}
        </Typography>
        <Typography>
          <b>Potentially Hazardeous: </b>
          {info.is_potentially_hazardous_asteroid ? "true" : "false"}
        </Typography>
      </Box>
    </div>
  );
};
export default NasaUserDetail;
