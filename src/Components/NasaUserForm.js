import { Box, Button, TextField } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { asteroidIdAction } from "../Actions/asteroidIdAction";
import { Link } from "react-router-dom";
import { asteroidAllIdAction } from "../Actions/asteroidAllIdAction";
import "../Common.scss";
const NasaUserForm = () => {
  const [id, setId] = useState(null);
  const [allAsteroid, setAllAsteroid] = useState([]);
  const dispatch = useDispatch();
  // onChange event
  const changeId = (e) => {
    setId(e.target.value);
  };
  // onClick event when user enter asteroid id
  const handleAsteroidId = () => {
    dispatch(asteroidIdAction(id));
  };
  // useEffect for calling all user id
  useEffect(() => {
    const fetch = async () => {
      const data = await asteroidAllIdAction();
      setAllAsteroid(data.near_earth_objects);
    };
    fetch();
  }, []);
  // getting random number and dispatch
  const handleRandomId = () => {
    const id = allAsteroid.map(({ id }) => id);
    console.log(id);
    const number = Math.ceil(Math.abs(Math.random() * id.length));
    dispatch(asteroidIdAction(id[number]));
  };
  // Nasa User Form
  return (
    <div className="div-screen">
      <Box className="box-form">
        <h1>Nasa User Form</h1>
        <TextField
          name={id}
          variant="outlined"
          color="primary"
          onChange={changeId}
        />
        <Link
          to={{ pathname: "/NasaUserDetail" }}
          style={{ textDecoration: "none" }}
        >
          <Button
            disabled={id === null || id.length <= 5}
            onClick={handleAsteroidId}
            variant="outlined"
            color="primary"
          >
            Submit
          </Button>
        </Link>
        <Link
          to={{ pathname: "/NasaUserDetail" }}
          style={{ textDecoration: "none" }}
        >
          <Button onClick={handleRandomId} variant="outlined" color="primary">
            Fetch Random Asteroid
          </Button>
        </Link>
      </Box>
    </div>
  );
};
export default NasaUserForm;
