import { asteroidType } from "../Actions/Type/asteroidType";

const initialState = {
  data: [],
};
export const asteroidReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case asteroidType:
      return {
        ...state,
        data: payload,
      };
    default:
      return state;
  }
};
