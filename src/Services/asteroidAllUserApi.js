import axios from "axios";

export const asteroidAllUserApi = () => {
  return axios.get(`${process.env.REACT_APP_API_URL_RANDOM}`);
};
