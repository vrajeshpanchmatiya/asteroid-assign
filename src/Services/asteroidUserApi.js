import axios from "axios";

export const asteroidUserApi = (id) => {
  return axios.get(
    `${process.env.REACT_APP_API_URL_FIXED}${id}?${process.env.REACT_APP_API_URL_KEY}`
  );
};
